use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct User {
  pub access_key_id: Option<String>,
  pub confirmed_at: Option<String>,
  pub email: String,
  pub id: u32,
  pub inserted_at: String,
  pub roles: Vec<String>,
  pub uuid: String,
}
