use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct CredentialContent {
  pub key: String,
  pub value: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Credential {
  pub inserted_at: String,
  pub id: i32,
  pub key: String,
  pub value: String,
}
