mod common;
mod credential;
mod user;
mod worker;
mod worker_definition;
mod workflow;
mod workflow_definition;
mod workflow_instance;

pub use common::{
  Icon, Identifier, Job, JobsStatus, ListItem, NotificationHook, NotificationHookCondition,
  Parameter, ParameterType, Requirement, StartParameter, StartParameterType, Step, StepMode, Store,
  WorkflowDuration,
};
pub use credential::{Credential, CredentialContent};
pub use user::User;
pub use worker::Worker;
pub use worker_definition::{WorkerDefinition, WorkerDescription};
pub use workflow::{SchemaVersion, Workflow};
pub use workflow_definition::{NotificationHooks, WorkflowDefinition};
pub use workflow_instance::{Status, WorkflowInstance};

#[macro_use]
extern crate lazy_static;

pub(crate) fn is_false(value: &bool) -> bool {
  !*value
}
