use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "notification")]
pub struct NotificationWithoutCondition {
  /// Label for the Notification
  pub label: String,
  /// Endpoint for the Notification
  pub endpoint: String,
  /// Credentials for the Notification
  pub credentials: Option<String>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "notification")]
pub struct NotificationHook {
  /// Label for the Notification
  pub label: String,
  /// Endpoint for the Notification
  pub endpoint: String,
  /// Credentials for the Notification
  pub credentials: Option<String>,
  /// Condition to execute the Notification
  pub conditions: Vec<NotificationHookCondition>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum NotificationHookCondition {
  JobCompleted,
  JobError,
  WorkflowCompleted,
  WorkflowError,
}
