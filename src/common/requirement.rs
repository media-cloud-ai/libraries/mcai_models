use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "requirement", rename_all = "snake_case")]
pub struct Requirement {
  #[serde(default)]
  pub paths: Vec<String>,
}
