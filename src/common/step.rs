use super::{Icon, JobsStatus, Parameter, StepMode};
use crate::ParameterType;
use mcai_types::Coordinates;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// Definition of a step
#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "step")]
pub struct Step {
  /// The icon for this step
  pub icon: Icon,
  /// Unique identifier of this step in a workflow
  pub id: u32,
  /// Displayed name of this step
  pub label: String,
  /// Queue name for this step, it select the worker
  pub name: String,
  /// List of all parameters for this step
  pub parameters: Vec<Parameter>,
  /// Mode for this step
  #[serde(default, skip_serializing_if = "StepMode::is_default")]
  pub mode: StepMode,
  /// Name of the variable which contain data ranges to process splitted media by part
  #[serde(default, skip_serializing_if = "Option::is_none")]
  pub multiple_jobs: Option<String>,
  /// Expression which requires to return a boolean. On false condition, the step will be skipped.
  #[serde(default, skip_serializing_if = "Option::is_none")]
  pub condition: Option<String>,
  /// Skip generation of a destination path parameter, it requires to add it in parameters
  #[serde(default, skip_serializing_if = "crate::is_false")]
  pub skip_destination_path: bool,
  /// Reference(s) of parent steps
  /// It is used to generate the input paths list from all parents destination paths
  #[serde(default, skip_serializing_if = "Vec::is_empty")]
  pub parent_ids: Vec<u32>,
  /// Reference(s) of required steps to start the process of that step
  #[serde(default, skip_serializing_if = "Vec::is_empty")]
  pub required_to_start: Vec<u32>,
  /// Overwrite the default workdir for this step
  #[serde(default, skip_serializing_if = "Option::is_none")]
  pub work_dir: Option<String>,
  /// Jobs status for this step
  #[serde(default, skip_serializing_if = "Option::is_none")]
  pub jobs: Option<JobsStatus>,
  /// Coordinates for this step
  #[serde(default, skip_serializing_if = "Option::is_none")]
  pub coordinates: Option<Coordinates>,
}

impl Step {
  pub fn add_parent(&mut self, parent_id: &u32) {
    // Check if parent already set
    if !self.parent_ids.contains(parent_id) {
      // Add parent_id
      self.parent_ids.push(*parent_id);
    }
  }

  pub fn add_required(&mut self, required_id: &u32) {
    if !self.required_to_start.contains(required_id) {
      self.required_to_start.push(*required_id);
    }
  }

  pub fn set_parameter(&mut self, field_name: &str, new_value: ParameterType) -> bool {
    if let Some(parameter) = self
      .parameters
      .iter_mut()
      .find(|parameter| parameter.id.content == *field_name)
    {
      match &mut parameter.kind {
        ParameterType::ArrayOfStrings { value, .. } => {
          *value = new_value.get_string_array().unwrap_or_default();
        }
        ParameterType::Boolean { value, .. } => {
          *value = new_value.get_bool();
        }
        ParameterType::Integer { value, .. } => {
          *value = new_value.get_integer();
        }
        ParameterType::Number { value, .. } => {
          *value = new_value.get_number();
        }
        ParameterType::Requirements { value, .. } => {
          *value = new_value.get_requirement();
        }
        ParameterType::String { value, .. } | ParameterType::Template { value, .. } => {
          *value = new_value.get_string();
        }
        ParameterType::Extended {
          value: parameter_value,
          ..
        } => {
          if let ParameterType::Extended { value, .. } = new_value {
            *parameter_value = value;
          }
        }
        _ => {}
      }
      true
    } else {
      false
    }
  }
}
