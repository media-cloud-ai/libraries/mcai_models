use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "rights", rename_all = "snake_case")]
pub struct Right {
  pub action: RightAction,
  pub groups: Vec<String>,
}

#[derive(Clone, Debug, Deserialize, JsonSchema, PartialEq, Serialize)]
#[serde(rename = "action", rename_all = "snake_case")]
pub enum RightAction {
  Abort,
  Create,
  Delete,
  Retry,
  Update,
  View,
}
