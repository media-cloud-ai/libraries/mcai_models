use regex::Regex;
use schemars::{
  gen::SchemaGenerator,
  schema::{InstanceType, Schema, SchemaObject, StringValidation},
  JsonSchema,
};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::fmt;

static ICON_REGEX: &str = "^([3a-z_]+)$";

lazy_static! {
  static ref REGEX: Regex = Regex::new(ICON_REGEX).unwrap();
}

#[derive(Clone, Debug, PartialEq)]
pub struct Icon {
  pub icon: Option<String>,
}

impl Icon {
  pub fn new(icon: Option<String>) -> Result<Self, String> {
    if let Some(icon) = &icon {
      if !REGEX.is_match(icon) {
        return Err(format!(
          "{} does not match the icon regex ({})",
          icon, ICON_REGEX
        ));
      }
    }
    Ok(Icon { icon })
  }
}

impl fmt::Display for Icon {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if let Some(icon) = &self.icon {
      f.write_str(icon)
    } else {
      Ok(())
    }
  }
}

impl<'de> Deserialize<'de> for Icon {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: Deserializer<'de>,
  {
    let icon = String::deserialize(deserializer)?;
    Icon::new(Some(icon)).map_err(serde::de::Error::custom)
  }
}

impl Serialize for Icon {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    if let Some(icon) = &self.icon {
      serializer.serialize_str(icon)
    } else {
      serializer.serialize_unit()
    }
  }
}

impl JsonSchema for Icon {
  fn schema_name() -> String {
    "icon".to_owned()
  }

  fn json_schema(_: &mut SchemaGenerator) -> Schema {
    SchemaObject {
      instance_type: Some(InstanceType::String.into()),
      string: Some(Box::new(StringValidation {
        pattern: Some(ICON_REGEX.to_owned()),
        ..Default::default()
      })),
      ..Default::default()
    }
    .into()
  }

  fn is_referenceable() -> bool {
    false
  }
}
