# MCAI Models

Models for the Media Cloud AI project

# Models included in this crate
- [x] Credential
- [x] Job
- [x] User
- [x] WorkerDefinition
- [x] WorkflowDefinition
- [x] Workflow
